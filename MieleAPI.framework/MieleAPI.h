//  MieleAPI.h
//  MieleAPI
//
//  Created by Alessandro Bettarini on 11 Dec 2020

#import <Foundation/Foundation.h>

//! Project version number for MieleAPI.
FOUNDATION_EXPORT double MieleAPIVersionNumber;

//! Project version string for MieleAPI.
FOUNDATION_EXPORT const unsigned char MieleAPIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MieleAPI/PublicHeader.h>

#import <MieleAPI/PluginFilter.h>
