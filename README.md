#  Plugin System

Copyright © 2020 Alessandro Bettarini. All rights reserved.

License GPLv3.0 -- see [License File](LICENSE)

This project provides a test bed for a macOS application being able to use dedicated "plugins"

The motivation for this project came about in Dec 2020 while testing the plugin system of the application Miele-LXIV. The problem was that the "sandboxed" build of the app was unable to "load" its own plugins after they were downloaded from the Internet. The "not-sandboxed" build didn't suffer from this problem. To be noted that both "sandboxed" and "not-sandboxed" version didn't have any problem loading a plugin installed locally. This is because if a sandboxed application downloads any file, this file gets the extended attribute `com.apple.quarantine`. The solution currently under investigation is to **notarize** the plugin.

This functionality is reproduce here, hoping that it will be easier to find a solution with a simplified project.

---
## Test a single plugin

The application window is divided in three areas

- **top**

	- a list of the plugins available to be downloaded
	- a button to do three actions 
		- download the zipped plugin to `/temp` directory,
		- unzip it
		- install: move it from `/temp` to the `Application Support` directory

- **middle**

	- a text label shows the location of the `Application Support` directory where plugins are installed. This location is opened in Finder when the refresh button is clicked. This is convenient for uninstalling a plugin, by simply deleting it using Finder.
	-  TODO: add an "Uninstall plugin" button
	- a list of plugins already installed locally, with a "refresh list" button
	- a button to **load** the selected plugin.
		-  If the plugin was downloaded (as opposed to copied into place locally) the problem under investigation is reproduced.
		-  the application's main menu is also updated to include the plugin's menu entries
		-  TODO: update the application's toolbar

- **bottom**

	- a button to run the plugin. The plugin will be invoked for each of its menu entries.

---
## Test multiple plugins
- This extended functionality goes beyond the immediate task of finding a solution to the problem ooutlined above.
- The goal is to mimic more of the plugin system as implemented in the application Miele-LXIV.
- To test with this option you need to define the compiler macro `WITH_PLUGIN_MANAGER`. This is currently under development.

