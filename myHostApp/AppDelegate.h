//  AppDelegate.h
//  myHostApp
//
//  Created by Alessandro Bettarini on 10 Dec 2020

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate, NSURLDownloadDelegate>

@property (weak) IBOutlet NSPopUpButton *pluginsListPopUp;
@property (weak) IBOutlet NSPopUpButton *installedListPopUp;
@property (weak) IBOutlet NSTextField *installedPath;

@property (weak) IBOutlet NSMenu *filtersMenu;
@property (weak) IBOutlet NSMenu *roisMenu;
@property (weak) IBOutlet NSMenu *othersMenu;
@property (weak) IBOutlet NSMenu *dbMenu;

- (IBAction)downloadOnePlugin:(id)sender;
- (IBAction)loadOnePlugin:(id)sender;
- (IBAction)runOnePlugin:(id)sender;
- (IBAction)refreshInstalledList:(id)sender;

@end

