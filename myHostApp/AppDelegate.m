//  AppDelegate.m
//  myHostApp
//
//  Created by Alessandro on 10/12/20.

#import "AppDelegate.h"
#import "PluginFilter.h"
#import "PluginManager.h"

#ifdef MORE_DETAILED_IMPLEMENTATION
// class variable
static PluginManager *pluginManager = nil;
#endif

@interface AppDelegate ()
{
    NSBundle *pluginBundle;
    NSMutableDictionary *availablePluginsD;
    NSString *downloadedFilePath;
    ViewerController* vC;
}

+ (void)movePluginFromPath:(NSString*)sourcePath
                    toPath:(NSString*)destinationPath;
- (NSString * )getInstalledPluginDir;

@property (strong) IBOutlet NSWindow *window;
@end

#pragma mark -

@implementation AppDelegate

// See PluginManager.mm:1020
+ (void)movePluginFromPath:(NSString*)sourcePath
                    toPath:(NSString*)destinationPath;
{
    NSLog(@"%s\n\t %@\n\t %@", __FUNCTION__, sourcePath, destinationPath);

    if ([sourcePath isEqualToString:destinationPath])
        return;
    
    BOOL isDir;
    BOOL srcOk = [[NSFileManager defaultManager] fileExistsAtPath:sourcePath isDirectory:&isDir];

    NSLog(@"%s src exists:%d, isDir:%d", __FUNCTION__, srcOk, isDir);

    if (![[NSFileManager defaultManager] fileExistsAtPath:[destinationPath stringByDeletingLastPathComponent]])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:[destinationPath stringByDeletingLastPathComponent]
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }

    {
        NSMutableArray *args = [NSMutableArray array];
        [args addObject:@"-f"];
        [args addObject:sourcePath];
        [args addObject:destinationPath];
        NSTask *task = [NSTask launchedTaskWithLaunchPath: @"/bin/mv" arguments: args];
        [task waitUntilExit];
        NSLog(@"mv task returned status:%d (0 = success)", [task terminationStatus]);
    }
    
    bool destExists = [[NSFileManager defaultManager] fileExistsAtPath: destinationPath];
    NSLog(@"destination exists: %d", destExists);

#if 0
    {
        // Unfortunately the following doesn't work, we get "Operation not permitted"
        NSString *pluginName = [sourcePath lastPathComponent];
        NSString *installedPlugin = [destinationPath stringByAppendingPathComponent:pluginName];
        NSLog(@"%s, installedPlugin: %@", __FUNCTION__, installedPlugin);

        NSMutableArray *args = [NSMutableArray array];
        [args addObject:@"-d"];
        [args addObject:@"-r"];
        [args addObject:@"com.apple.quarantine"];
        [args addObject:installedPlugin];
        NSTask *task = [NSTask launchedTaskWithLaunchPath: @"/usr/bin/xattr" arguments: args];
        [task waitUntilExit];
        NSLog(@"xattr task returned status:%d (0 = success)", [task terminationStatus]);
    }
#endif
    NSLog(@"%s END", __FUNCTION__);
}

// NSApplicationSupportDirectory
//  no sandbox
//      /Users/lxiv/Library/Application Support
//  sandbox
//      /Users/lxiv/Library/Containers/bettar.myHostApp/Data/Library/Application Support
- (NSString *) getInstalledPluginDir
{
    //return NSTemporaryDirectory(); // cannot load bundle from here

    return NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES).firstObject;
}

#pragma mark - NSApplication

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
#if 1
    NSLog(@"NSTemporaryDirectory()_: %@", NSTemporaryDirectory());
    NSLog(@"NSHomeDirectory()______: %@", NSHomeDirectory());
    NSLog(@"Library directory______: %@", NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES).firstObject);
    NSLog(@"Document directory_____: %@", NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject);
    NSLog(@"Application Support dir: %@", NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES).firstObject);
    NSLog(@"Shared Public dir______: %@", NSSearchPathForDirectoriesInDomains(NSSharedPublicDirectory, NSUserDomainMask, YES).firstObject);
#endif

#ifdef MORE_DETAILED_IMPLEMENTATION
    // 3129
    // This will "discover" the installed plugins
    pluginManager = [[PluginManager alloc] init];
#endif

    // Get available plugins list from bundle
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"plugins" ofType:@"plist"];
    availablePluginsD = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    NSLog(@"Plugins d. available for downloading:\n%@", availablePluginsD);

//    NSArray *availableArray =[NSArray arrayWithContentsOfFile:plistPath];
//    NSLog(@"Plugins a. available for downloading:\n%@", availableArray);
    
    // See PluginManagerController.mm:330
    [_pluginsListPopUp removeAllItems];
    for (id loopItem in availablePluginsD.allValues) {
        [_pluginsListPopUp addItemWithTitle:[loopItem objectForKey:@"name"]];
    }
    
    [self refreshInstalledList:nil];
    
    // Show where we are installing the plugins
    [_installedPath setStringValue:[self getInstalledPluginDir]];
    
    vC = [ViewerController new];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

#pragma mark - Actions

// See PluginManagerController.mm:436
// See PluginManagerController.mm:466
// download zip
// Miele-LXIV downloads to temp
// then in downloadDidFinish installDownloadedPluginAtPath is called to unzip to "App Support"
- (IBAction)downloadOnePlugin:(id)sender
{
    NSInteger selIdx = _pluginsListPopUp.indexOfSelectedItem;
    if (selIdx == -1)
        return;

//    NSString *selName = _pluginsListPopUp.titleOfSelectedItem;
//    NSLog(@"selected: %ld <%@>", (long)selIdx, selName);
    NSArray * availablePluginsA = availablePluginsD.allValues;
    NSDictionary * selPluginD = availablePluginsA[ selIdx];
    NSDictionary * downloadUrlDict = [selPluginD valueForKey:@"download_url"];
//    NSLog(@"url: <%@>", [selPluginD valueForKey:@"url"]);

    NSString *host = [downloadUrlDict valueForKey:@"host"];
    NSString *path = [downloadUrlDict valueForKey:@"path"];
    NSString *scheme = [downloadUrlDict valueForKey:@"scheme"];
    //NSLog(@"scheme: <%@>, host: <%@>, path: <%@>", scheme, host, path);

    NSURLComponents *urlComponents = [NSURLComponents new];
    [urlComponents setScheme:scheme];
    [urlComponents setHost:host];
    [urlComponents setPath:path];
    NSURL *url = urlComponents.URL;

    // Note: we cannot specify the no quarantine attribute here,
    // because getResourceValue and setResourceValue are only for file URLs
    
    NSString *lastComponent = [[path lastPathComponent] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    downloadedFilePath = [NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), lastComponent];

    NSLog(@"%s, url:<%@>\nas file:<%@>", __PRETTY_FUNCTION__, url.absoluteString, downloadedFilePath);

    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLDownload *download = [[NSURLDownload alloc] initWithRequest:request delegate:self];

    [download setDestination: downloadedFilePath allowOverwrite:YES];
}

// See PluginManager.mm:633
- (IBAction)loadOnePlugin:(id)sender
{
    NSString *pluginName = _installedListPopUp.titleOfSelectedItem;
    NSString *pluginDirDir = [self getInstalledPluginDir];
    NSString *pluginPath = [pluginDirDir stringByAppendingPathComponent:pluginName];
    pluginPath = [pluginPath stringByAppendingPathExtension:@"mieleplugin"];

    BOOL isDir;
    if (![[NSFileManager defaultManager] fileExistsAtPath:pluginPath isDirectory:&isDir]) {
        NSLog(@"%s line %d, %@ doesn't exist:<%@>", __PRETTY_FUNCTION__, __LINE__,
              (isDir) ? @"dir" : @"file", pluginPath);
        return;
    }

#ifdef MORE_DETAILED_IMPLEMENTATION
    [PluginManager loadPluginAtPath:pluginPath];
#else // MORE_DETAILED_IMPLEMENTATION
    pluginBundle = [NSBundle bundleWithPath: pluginPath];

#if 1
    NSError *err;
    if (![pluginBundle loadAndReturnError:&err]) {
        NSLog(@"%s line %d, %@", __PRETTY_FUNCTION__, __LINE__, err);
        return;
    }
#else
    if (![pluginBundle load]) {
        NSLog(@"%s Bundle code loading failed", __PRETTY_FUNCTION__);
        return;
    }
#endif

    Class filterClass = [pluginBundle principalClass];
    if (!filterClass) {
        NSLog(@"%s line %d, principalClass not found", __PRETTY_FUNCTION__, __LINE__);
        //return;
    }

    // First try CFBundleVersion
    NSString *version = [[pluginBundle infoDictionary] valueForKey: (NSString*) kCFBundleVersionKey];
    
    // If CFBundleVersion is missing, try CFBundleShortVersionString
    if (version == nil) {
        NSLog(@"%s line %d, CFBundleVersion not found", __PRETTY_FUNCTION__, __LINE__);
        version = [[pluginBundle infoDictionary] valueForKey: @"CFBundleShortVersionString"];
    }

    NSLog( @"Loaded: %@\n\t class: %@\n\t vers: %@\n\t path: <%@>", pluginName, filterClass, version, pluginPath);
#endif // MORE_DETAILED_IMPLEMENTATION
}

- (IBAction)refreshInstalledList:(id)sender
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *extractDirPath = [self getInstalledPluginDir];
    //NSLog(@"extractDirPath: %@", extractDirPath);
    NSError *err;
    NSArray *extractsList = [fm contentsOfDirectoryAtPath: extractDirPath error: &err];
    //NSLog(@"extractsList: %@", extractsList);

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pathExtension='mieleplugin'"];
    NSArray *pluginDiscovered = [extractsList filteredArrayUsingPredicate:predicate];
    NSLog(@"pluginDirs: %@", pluginDiscovered);

    [_installedListPopUp removeAllItems];
    for (id loopItem in pluginDiscovered) {
        [_installedListPopUp addItemWithTitle:[loopItem stringByDeletingPathExtension]];
    }
    
    // Reveal in finder
    if (sender)
        [[NSWorkspace sharedWorkspace] selectFile: extractDirPath
                         inFileViewerRootedAtPath: @""];
}

- (IBAction)runOnePlugin:(id)sender
{
    // See PluginManager.mm:634
    Class filterClass = [pluginBundle principalClass];
    
    if (!pluginBundle) {
        NSLog(@"%s bundle not loaded", __PRETTY_FUNCTION__);
        return;
    }
    
    // Instantiate the plugin
//    id filter = [[filterClass alloc] init];
    id filter = [filterClass filter]; // See PluginManager.mm:680
    [filter prepareFilter: vC];
    
    NSArray *menuTitles = [[pluginBundle infoDictionary] objectForKey:PINFO_MENU_TITLES]; // array

    NSLog(@"plugin description: %@", filter);
    // Info.plist
    NSLog(@"\t exe:%@\n\t type:%@\n\t menu:%@\n\t toolbar:%@\n\t tooltips:%@\n\t bundle name:%@",
          [[pluginBundle infoDictionary] objectForKey:PINFO_CF_BUNDLE_EXE],
          [[pluginBundle infoDictionary] objectForKey:PINFO_TYPE],
          menuTitles, // array
          [[pluginBundle infoDictionary] objectForKey:PINFO_TOOLBAR_NAMES], // array
          [[pluginBundle infoDictionary] objectForKey:PINFO_TB_TOOLTIPS], // dic
          //[[pluginBundle infoDictionary] objectForKey:PINFO_REQUIREMENTS],
          //[[pluginBundle infoDictionary] objectForKey:PINFO_FILE_FORMATS]
          [[pluginBundle infoDictionary] objectForKey:PINFO_CF_BUNDLE_NAME]
          );
    //NSLog(@"infoDictionary:%@", [pluginBundle infoDictionary]);

    if ( [filterClass instancesRespondToSelector:@selector(filterImage:)]) {
        for (NSString *menuTitle in menuTitles) {
            if ([menuTitle isEqual:PINFO_MENU_ITEM_SEPARATOR])
                continue;

            long result = [filter filterImage:menuTitle];
            NSLog(@"filter result: %ld (0 = success)", result);
        }
    }
}

#pragma mark - NSURLDownloadDelegate

- (void)downloadDidBegin:(NSURLDownload *)download
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

// See PluginManagerController.mm:492
// installDownloadedPluginAtPath 553
// - unzip
// - movePluginFromPath
//      executeCommand:@"/bin/mv -f"
//      executeCommand:@"/bin/cp -f -R"
- (void)downloadDidFinish:(NSURLDownload *)download
{
    NSLog(@"%s\n\t %@\n\t %@", __PRETTY_FUNCTION__,
          downloadedFilePath, // valid
          download);    // nil

#if 1
    if (@available(macOS 10.10, *)) {
        NSError *err = nil;
        NSURL *url = [NSURL fileURLWithPath:downloadedFilePath];
        BOOL ok;
        NSMutableDictionary* properties = [NSMutableDictionary new];
        ok = [url getResourceValue: &properties
                            forKey: NSURLQuarantinePropertiesKey
                             error: &err];
        NSLog(@"%s line %d, ok:%d, err:%@, properties:\n%@",
              __FUNCTION__, __LINE__, ok, err, properties);

        ok = [url setResourceValue:[NSNull null] forKey:NSURLQuarantinePropertiesKey error:&err];
        NSLog(@"%s line %d, ok:%d", __FUNCTION__, __LINE__, ok);
    }
#endif

    NSString *pluginPath = downloadedFilePath;

    NSString *dowloadedFileExtension = [downloadedFilePath pathExtension];

    if ([dowloadedFileExtension isEqualTo:@"zip"])
    {
        @try
        {
            NSTask *aTask = [[NSTask alloc] init];
            NSMutableArray *args = [NSMutableArray array];

            [args addObject:@"-o"];
            [args addObject:downloadedFilePath];
            [args addObject:@"-d"];
            [args addObject:[downloadedFilePath stringByDeletingLastPathComponent]];
            [aTask setLaunchPath:@"/usr/bin/unzip"];
            [aTask setArguments:args];
            [aTask launch];
            while ([aTask isRunning])
                [NSThread sleepForTimeInterval: 0.1];
            
            [aTask waitUntilExit];
            NSLog(@"Unzip task returned status:%d", [aTask terminationStatus]);
        }
        @catch (NSException *e)
        {
            NSLog( @"***** exception in %s: %@", __PRETTY_FUNCTION__, e);
        }
        
        // cleanup zip file
        pluginPath = [downloadedFilePath stringByDeletingPathExtension];
        [[NSFileManager defaultManager] removeItemAtPath:downloadedFilePath error:nil];
    }
    else if ([dowloadedFileExtension isEqualTo:@"xip"])
    {
        // /usr/bin/xip --expand inputfile
        @try
        {
            NSTask *aTask = [[NSTask alloc] init];
            NSMutableArray *args = [NSMutableArray array];

            [args addObject:@"--expand"];
            [args addObject:downloadedFilePath];
            [aTask setArguments:args];

            [aTask setLaunchPath:@"/usr/bin/xip"];
            [aTask launch];
            while ([aTask isRunning])
                [NSThread sleepForTimeInterval: 0.1];
            
            [aTask waitUntilExit];
            //xip: error: The archive “myPlugin.mieleplugin.xip” does not come from Apple.
            NSLog(@"Unzip task returned status:%d", [aTask terminationStatus]);
        }
        @catch (NSException *e)
        {
            NSLog( @"***** exception in %s: %@", __PRETTY_FUNCTION__, e);
        }
        
        // cleanup zip file
        pluginPath = [downloadedFilePath stringByDeletingPathExtension];
        [[NSFileManager defaultManager] removeItemAtPath:downloadedFilePath error:nil];
    }
    else {
        NSLog(@"Not a zip file");
    }

    // Install the plugin
    NSString *installDirectoryPath = [self getInstalledPluginDir];
    NSLog(@"Install: copy from to\n\t %@\n\t %@", pluginPath, installDirectoryPath);
    
    // Install the plugin
    [AppDelegate movePluginFromPath:pluginPath toPath: installDirectoryPath];
}

- (void)download:(NSURLDownload *)download didFailWithError:(NSError *)error
{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, [error localizedDescription]);
}
@end
