//
//  PluginManager.m
//  myHostApp
//
//  Created by Alessandro on 18/12/20.
//

#import "PluginManager.h"
#import "PluginFilter.h"

// Class variables
// 36
static NSMutableDictionary *installedPlugins = nil;

// 45
static NSMutableDictionary *pluginsNames = nil;

@interface PluginManager ()
{
}
@end

@implementation PluginManager

// 154
+ (NSMutableDictionary*) installedPlugins
{
    return installedPlugins;
}

// 449
- (instancetype)init
{
    self = [super init];
    if (self) {
        [PluginManager discoverPlugins];
    }

    return self;
}


// 585
+ (void) loadPluginAtPath: (NSString*) path
{
    // 622
    NSBundle *pluginBundle = [NSBundle bundleWithPath: path];

#if 1
    NSError *err;
    if (![pluginBundle loadAndReturnError:&err]) {
        NSLog(@"%s line %d, %@", __PRETTY_FUNCTION__, __LINE__, err);
        return;
    }
#else
    if (![pluginBundle load]) {
        NSLog(@"%s Bundle code loading failed", __PRETTY_FUNCTION__);
        return;
    }
#endif
    // 634
    Class filterClass = [pluginBundle principalClass];

    // 670
    PluginFilter *filter = [filterClass filter];
}

// 714
+ (void) discoverPlugins
{
    // 790
    installedPlugins = [[NSMutableDictionary alloc] init];
    // 795
    pluginsNames = [[NSMutableDictionary alloc] init];
}

// 1386
+ (NSArray*)pluginsList;
{
    // 1401
    NSMutableArray *plugins = [NSMutableArray array];
    return plugins;
}

@end
