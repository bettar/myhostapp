//
//  ViewerController.h
//  MieleAPI.framework
//
//  Created by Alessandro on 11/12/20.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface ViewerController : NSWindowController

// 407
+ (NSMutableArray*) getDisplayed2DViewers;

// 449
// Display a Wait window with the message
- (id) startWaitWindow :(NSString*) message;

// 462
- (void) endWaitWindow:(id) waitWindow;

// 466
// Refresh the current displayed image
- (void) needsDisplayUpdate;

// 474
- (NSView*) imageView;

// 479
- (NSMutableArray*) pixList;

// 647
- (short) maxMovieIndex;

// 664
- (ViewerController*) copyViewerWindow;

@end

NS_ASSUME_NONNULL_END
