//
//  PluginManager.h
//  myHostApp
//
//  Created by Alessandro on 18/12/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define PLUGIN_EXTENSION        @"mieleplugin"

// InfoPlist.strings (localizable)
#define PINFO_CF_BUNDLE_NAME    @"CFBundleName"

// Info.plist
#define PINFO_CF_BUNDLE_EXE     @"CFBundleExecutable"
#define PINFO_REQUIREMENTS      @"Requirements"
#define PINFO_TYPE              @"pluginType"
#define PINFO_MENU_TITLES       @"MenuTitles"   // array
#define PINFO_TOOLBAR_NAMES     @"ToolbarNames" // array
#define PINFO_FILE_FORMATS      @"FileFormats"
#define PINFO_ALLOW_TB_ICON     @"allowToolbarIcon" // bool
#define PINFO_TB_ICON           @"ToolbarIcon"
#define PINFO_TB_TOOLTIPS       @"ToolbarToolTips"  // dictionary

// For PINFO_TYPE:
#define PTYPE_REPORT            @"Report"       // invoked by executeFilterDB:
#define PTYPE_DATABASE          @"Database"     // invoked by executeFilterDB:
#define PTYPE_FUSION_FILTER     @"fusionFilter" // invoked by endBlendingType:
#define PTYPE_IMAGE_FILTER      @"imageFilter"  // invoked by executeFilter:
#define PTYPE_PRE_PROCESS       @"Pre-Process"  // invoked by executeFilter:
#define PTYPE_ROI_TOOL          @"roiTool"      // invoked by executeFilter:
#define PTYPE_OTHER             @"other"        // invoked by executeFilter:

// Put this line by itseslf in the plugin INFO.plist to have a separator in the menu entries
#define PINFO_MENU_ITEM_SEPARATOR   @"(-"

@interface PluginManager : NSObject

// 78
+ (void) loadPluginAtPath: (NSString*) path;

// 98
+ (NSArray*)pluginsList;

@end

NS_ASSUME_NONNULL_END
