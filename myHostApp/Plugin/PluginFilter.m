//  PluginFilter.m
//  MieleAPI
//
//  Created by Alessandro on 11 Dec 2020

#import <Cocoa/Cocoa.h>
#import "PluginFilter.h"

@implementation PluginFilter

+ (PluginFilter *)filter
{
    return [[self alloc] init];
}

// 38
- (void) willUnload
{
    // Subclass this function if needed: Plugin will unload : prepare for dealloc: release memory and kill sub-process
}

// 50
- (void) initPlugin
{
    return;
}

// 55
- (void)setMenus {  // Opportunity for plugins to make Menu changes if necessary
    return;
}

// 58
- (long) prepareFilter:(ViewerController*) vC
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    viewerController = vC;
    return 0;
}

// 67
- (BOOL) isCertifiedForMedicalImaging
{
    return NO;
}

// 72
- (ViewerController*) duplicateCurrent2DViewerWindow
{
    return [viewerController copyViewerWindow];
}

// 77
- (NSArray*) viewerControllersList
{
    return [ViewerController getDisplayed2DViewers];
}

// 82
- (long) filterImage:(NSString*) menuName
{
    NSLog(@"%s %d Error, you should not be here!: %@", __FUNCTION__, __LINE__, menuName);
    return -1;
}

// 88
- (long) processFiles: (NSMutableArray*) files
{
    return 0;
}

// 93
- (id) report: (NSManagedObject*) study action:(NSString*) action
{
    return 0;
}

// 98
// Following stubs are to be subclassed.  Included here to remove compile-time warning messages.

- (id)reportDateForStudy: (NSManagedObject*)study {
    return nil;
}

// 104
- (BOOL)deleteReportForStudy: (NSManagedObject*)study {
    return NO;
}

- (BOOL)createReportForStudy: (NSManagedObject*)study {
    return NO;
}

@end
