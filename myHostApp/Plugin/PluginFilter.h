//
//  PluginFilter.h
//  MieleAPI
//
//  Created by Alessandro on 11 Dec 2020

#import <Foundation/Foundation.h>
#import "ViewerController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PluginFilter : NSObject
{
	ViewerController* viewerController;   // Current (frontmost and active) 2D viewer containing an image serie
}

+ (PluginFilter *)filter;

// FUNCTIONS TO SUBCLASS

/** This function is called to apply your plugin
 * Some plugins have more than a single menu item
 */
- (long) filterImage: (NSString*) menuName;

/** This function is the entry point of Pre-Process plugins */
- (long) processFiles: (NSMutableArray*) files;

/** This function is the entry point of Report plugins
* action = @"dateReport"    -> return NSDate date of creation or modification of the report, nil if no report available
* action = @"deleteReport"    -> return nil, delete the report
* action = @"openReport"   -> return nil, open and display the report, create a new one if no report available
*/
- (id) report: (NSManagedObject*) study action:(NSString*) action;

/** This function is called at the Miele-LXIV startup, if you need to do some memory allocation, etc. */
- (void) initPlugin;

/** This function is called if Miele-LXIV needs to kill the current running plugin, to install an update, for example. */
- (void) willUnload;

/** This function is called if Miele-LXIV needs to display a warning to the user about a non-certified plugin. */
- (BOOL) isCertifiedForMedicalImaging;

/** Opportunity for plugins to make Menu changes if necessary */
- (void)setMenus;

// UTILITY FUNCTIONS - Defined in the PluginFilter.m file

/** Return the complete lists of opened studies in Miele-LXIV */
/** NSArray contains an array of ViewerController objects */
- (NSArray*) viewerControllersList;

/** Create a new 2D window, containing a copy of the current series */
- (ViewerController*) duplicateCurrent2DViewerWindow;

// Following stubs are to be subclassed by report filters.  Included here to remove compile-time warning messages.
/** Stub is to be subclassed by report filters */
- (id)reportDateForStudy: (NSManagedObject*)study;
/** Stub is to be subclassed by report filters */
- (BOOL)deleteReportForStudy: (NSManagedObject*)study;
/** Stub is to be subclassed by report filters */
- (BOOL)createReportForStudy: (NSManagedObject*)study;

/** PRIVATE FUNCTIONS - DON'T SUBCLASS OR MODIFY */
- (long) prepareFilter:(ViewerController*) vC;
@end

NS_ASSUME_NONNULL_END
